import 'package:flutter/material.dart';
import 'package:grouped_list/grouped_list.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

void main() => runApp(WhtApp());

class WhtApp extends StatefulWidget {
  @override
  State<WhtApp> createState() => _WhtAppState();
}

class _WhtAppState extends State<WhtApp> {
  List<Message> messages=[
    Message(
      texto: "Recuerda que hoy voy a visitar a tus tíos.",
      fecha: DateTime.now().subtract(Duration(days: 5, minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Asi que llegaré tarde.",
      fecha: DateTime.now().subtract(Duration(days: 5, minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Ok mamá. Ve con mucho cuidado.",
      fecha: DateTime.now().subtract(Duration(days: 5, minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Me mandas mensaje cuando ya estes en la casa de mis tíos.",
      fecha: DateTime.now().subtract(Duration(days: 5, minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Vale, yo te mando mensaje. Nos vemos.",
      fecha: DateTime.now().subtract(Duration(days: 5, minutes: 1)),
      enviadoPorMi: false,
    ),


    Message(
      texto: "Ya vienes en camino?",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Si, voy hacia el metro.",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "OK. Vente con mucho cuidado.",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Se me olvidaba, puedes pasar a la panadería y comprar 4 piezas de pan?.",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Es que no paso el panadero con el pan jaja XD. Por favor uwu",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "No traigo mucho dinero :(",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Pero a ver para que me alcanza",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Bueno, entonces en un rato te vemos en la casa >o<.",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Ok. Los veo en un rato uwu",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Se me olvidaba, que piezas de pan compro?",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

    Message(
      texto: "Tu papá quiere una dona de chocolate",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Tu hermano quiere una concha de chocolate",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Y a mi traeme un taco de piña uwu",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: false,
    ),

    Message(
      texto: "Vale, anotado.",
      fecha: DateTime.now().subtract(Duration(minutes: 1)),
      enviadoPorMi: true,
    ),

  ];

  @override
  Widget build(BuildContext context) => MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
      appBar: AppBar(
        title:
        Row(
          children: [
            CircleAvatar(
              radius: 25,
              child: ClipOval(
                child: Image.network("https://png.pngtree.com/png-clipart/20190921/original/pngtree-mother-s-day-cartoon-mother-and-daughter-happy-life-reading-together-png-image_4699889.jpg")
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(14.8),
              child: Column(
                children: [
                  Text("Mamá",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 20),),
                  Text("En línea",
                  textAlign: TextAlign.left,
                  style: TextStyle(fontSize: 12),),
                ],
              ),
            ),

            ButtonTheme(
              minWidth: 20,
              height: 50,
              child: FloatingActionButton(
                  elevation: 0,
                  highlightElevation: 0,
                  backgroundColor: Color.fromARGB(250, 18, 140, 126),
                  child: Icon(
                    Icons.videocam,
                    size: 30,
                    color: Colors.white,
                  ),
                  onPressed: (){

              }),
            ),


            ButtonTheme(
              minWidth: 20,
              height: 50,
              child: FloatingActionButton(
                elevation: 0,
                highlightElevation: 0,
                backgroundColor: Color.fromARGB(250, 18, 140, 126),
                  child: Icon(
                    Icons.phone,
                    color: Colors.white,
                  ),
                  onPressed:(){

                  }),
            ),


            ButtonTheme(
              minWidth: 20,
              height: 50,
              child: FloatingActionButton(
                elevation: 0,
                highlightElevation: 0,
                backgroundColor: Color.fromARGB(250, 18, 140, 126),
                  child: Icon(
                    Icons.more_vert,
                    color: Colors.white,
                  ),
                  onPressed: (){

                  }),
            )
          ],
        ),

      backgroundColor: Color.fromARGB(250, 18, 140, 126),
        leading: MaterialButton(
          elevation: 0,
          highlightElevation: 0,
          child: Icon(
            Icons.arrow_back,
            size: 25,
            color: Colors.white,
          ),
          onPressed: () {

          },
        ),
      ),

      body:
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("img/fondo_wht.jpg"),
              fit: BoxFit.cover
            ),
          ),

          child: Column(
            children: [
              Expanded(child: GroupedListView<Message, DateTime>(
                padding: const EdgeInsets.all(8),
                reverse: true,
                order: GroupedListOrder.DESC,
                useStickyGroupSeparators: true,
                floatingHeader: true,
                elements: messages,
                groupBy: (message) => DateTime(
                  message.fecha.day,
                  message.fecha.month,
                  message.fecha.day,
                ),
                groupHeaderBuilder: (Message message) =>SizedBox(
                  height: 40,
                  child: Center(
                    child: Card(
                      color: Colors.white38,
                      child: Padding(
                        padding: const EdgeInsets.all(8),
                        child: Text(
                          DateFormat.yMMMd().format(message.fecha),
                          style: const TextStyle(color: Colors.white),
                        ),
                      ),
                    ),
                  ),
                ),
                itemBuilder: (context, Message message) =>Align(
                  alignment: message.enviadoPorMi
                    ? Alignment.centerRight
                    : Alignment.centerLeft,
                  child: Card(
                    elevation: 8,
                    child: Padding(
                      padding: const EdgeInsets.all(12),
                      child: Text(message.texto),
                    ),
                  ),
                ),
                ),
              ),

              Container(
                color: Colors.grey.shade300,
                padding: EdgeInsets.symmetric(horizontal: 8),
                height: 65,
                child: Row(
                  children: <Widget> [
                    IconButton(
                      onPressed: (){

                      },
                      icon: Icon(Icons.emoji_emotions_outlined),
                      iconSize: 25,
                      color: Colors.black54,
                    ),

                    Expanded(
                        child:TextField(
                          decoration: const InputDecoration(
                              contentPadding: EdgeInsets.all(12),
                              hintText: 'Mensaje'
                          ),
                          onSubmitted: (text){
                            final message = Message(
                              texto: text,
                              fecha: DateTime.now(),
                              enviadoPorMi: true,
                            );
                            setState(() {
                              messages.add(message);
                            });
                          },
                        ),
                    ),

                    IconButton(
                      onPressed: (){

                      },
                      icon: Icon(Icons.attach_file_outlined),
                      iconSize: 25,
                      color: Colors.black54,
                    ),

                    IconButton(
                      onPressed: (){

                      },
                      icon: Icon(Icons.camera_alt_outlined),
                      iconSize: 25,
                      color: Colors.black54,
                    ),

                    ButtonTheme(
                      minWidth: 50,
                      height: 20,
                      child: FloatingActionButton(
                          elevation: 0,
                          highlightElevation: 0,
                          backgroundColor: Color.fromARGB(250, 18, 140, 126),
                          child: Icon(
                            Icons.send,
                            color: Colors.white,
                          ),
                          onPressed: (){

                          }),
                    )



                  ],
                )
              )
            ]
          ),


        )


    )
  );

}

class Message{
  final String texto;
  final DateTime fecha;
  final bool enviadoPorMi;

  const Message({
    required this.texto,
    required this.fecha,
    required this.enviadoPorMi,
});
}

